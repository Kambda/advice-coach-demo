### Kambda Facebook Login Demo

To run the project please follow the next steps:

At the root directory of your project:

`npm install`

For **IOS**:

Run `react-native run-ios`

For **Android**:

Run `react-native run-android`

*Make sure you have the emulator, either for IOS or Android running.
**Install react-native globally if necessary.
