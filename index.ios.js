/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  NavigatorIOS
} from 'react-native';
import App from './app';

export default class KambdaDemo extends Component {
  render() {
    return (
      <NavigatorIOS
        initialRoute={{
          title: App().getTitle(),
          component: App().getInitialRoute
        }}
        style={{flex: 1}}
      />
    );
  }
}

AppRegistry.registerComponent('KambdaDemo', () => KambdaDemo);
