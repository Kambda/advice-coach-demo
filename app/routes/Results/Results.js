import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  BackAndroid,
  Platform,
  Text,
  View
} from 'react-native';
import update from 'immutability-helper';
import styles from './styles';

export default class Results extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoader: true,
      userData: null
    };
  }

  componentWillMount() {
    AsyncStorage.getItem('fbUserToken')
      .then((token) => {
        fetch('https://graph.facebook.com/v2.5/me?fields=email,name&access_token=' + token)
        .then((response) => response.json())
        .then((json) => {
          this.setState(update(this.state, { $merge: { showLoader: false, userData: json } }));
        })
        .catch((error) => {
          console.log(error);
        });
      });

      if (Platform.OS === 'android') {
        BackAndroid.addEventListener('hardwareBackPress', () => {
          this.props.navigator.pop();
          return true;
        });
      }
  }

  render() {
    var UserData = null;

    if (this.state.userData) {
        UserData = (<Text style={styles.userData}>
          {JSON.stringify(this.state.userData)}
        </Text>);
    }

    return (
      <View style={styles.container}>
        <ActivityIndicator
          animating={this.state.showLoader}
          size="large"
        />
        {UserData}
      </View>
    );
  }
}
