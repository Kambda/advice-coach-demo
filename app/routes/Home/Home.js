import React, { PropTypes, Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  AppRegistry,
  AsyncStorage,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import { FBLogin, FBLoginManager } from 'react-native-facebook-login';
import Results from '../Results';
import update from 'immutability-helper';
import styles from './styles';

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLogged: false
    };
  }

  componentWillMount() {
    AsyncStorage.getItem('fbUserToken')
      .then((token) => {
        if (token) {
          this.setState(update(this.state, { $merge: { isLogged: true } }));
        }
      });
  }

  onFacebookLogin(data) {
    AsyncStorage.setItem('fbUserToken', data.credentials.token)
      .then(() => {
        this.setState(update(this.state, { $merge: { isLogged: true } }));
      });
  }

  onFacebookLogout() {
    console.log('User logged out successfully.');
    AsyncStorage.removeItem('fbUserToken')
      .then(() => {
        this.setState(update(this.state, { $merge: { isLogged: false } }));
      });
  }

  onFacebookLoginError(data) {
    Alert.alert('Facebook login error.', data.description, {
      text: 'OK',
      onPress: () => console.log('OK pressed.')
    });
  }

  onFacebookPermissionsMissing(data) {
    console.log('Missing permissions. Take action.');
    console.log(data);
  }

  _onLoadUserDataView() {
    this.props.navigator.push({
      title: 'Results',
      component: Results
    });
  }

  render() {
    const ShowUserDataButton = this.state.isLogged ?
      ( <View>
          <TouchableHighlight style={styles.userDataButton}
          underlayColor="#239B56"
          onPress={this._onLoadUserDataView.bind(this)}>
            <Text style={styles.userDataButtonText}>Show User Data</Text>
          </TouchableHighlight>
       </View> ) :
      ( <View/> )

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Kambda Demo
        </Text>
        {ShowUserDataButton}
        <FBLogin
          style={styles.fbButton}
          permissions={['email', 'public_profile']}
          loginBehavior={FBLoginManager.LoginBehaviors.Native}
          onLogin={this.onFacebookLogin.bind(this)}
          onLogout={this.onFacebookLogout.bind(this)}
          onError={this.onFacebookLoginError.bind(this)}
          onPermissionsMissing={this.onFacebookPermissionsMissing.bind(this)}
        />
      </View>
    );
  }
}

Home.propTypes = {
  navigator: PropTypes.object
};

export default Home;
