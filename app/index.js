import React from 'react';
import Routes from './config/routes';

// We define with which route we start
export default function() {
  return {
    getInitialRoute: Routes.getHomeRoute().renderScene,
    getTitle: Routes.getHomeRoute().getTitle
  };
}
