import React, { Component } from 'react';
import Home from '../routes/Home';
import Results from '../routes/Results';

export const routes = {
  getHomeRoute() {
    return {
      renderScene(navigator) {
        if (navigator.navigator) {
          navigator = navigator.navigator;
        }

        return <Home navigator={navigator} />;
      },
      getTitle() {
        return 'Home';
      }
    };
  },
  getResultsRoute() {
    return {
      renderScene(navigator) {
        if (navigator.navigator) {
          navigator = navigator.navigator;
        }

        return <Results navigator={navigator} />;
      },
      getTitle() {
        return 'Results';
      }
    };
  }
};

export default routes;
