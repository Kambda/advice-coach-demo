/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Navigator
} from 'react-native';
import App from './app';
import Routes from './app/config/routes';

export default class KambdaDemo extends Component {
  _renderScene(route, navigator) {
    if (route.title === 'Home') {
      return Routes.getHomeRoute().renderScene(navigator);
    }
    if (route.title === 'Results') {
      return Routes.getResultsRoute().renderScene(navigator);
    }
  }

  render() {
    const routes = [
      { title:  Routes.getHomeRoute().getTitle() },
      { title:  Routes.getResultsRoute().getTitle() }
    ];

    return (
      <Navigator
        initialRoute={routes[0]}
        initialRouteStack={routes}
        renderScene={this._renderScene.bind(this)}
        style={{flex: 1}}
      />
    );
  }
}

AppRegistry.registerComponent('KambdaDemo', () => KambdaDemo);
